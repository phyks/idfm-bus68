#!/usr/bin/env python
import datetime
import os
import sys

import peewee
import requests

from playhouse.db_url import connect

db = connect(os.environ.get('DATABASE', 'sqlite:///bus_68_datapoints.db'))
DEBUG = os.environ.get('DEBUG', False)


class DataPoint(peewee.Model):
    class Meta:
        database = db

    datetime = peewee.DateTimeField(
        default=datetime.datetime.now()
    )
    lineDirection = peewee.CharField(max_length=255)
    code = peewee.CharField(max_length=255)
    time = peewee.CharField(max_length=255)


if __name__ == '__main__':
    db.connect()
    db.create_tables([DataPoint])

    LINE_ID = '100100068:68'  # Bus 68
    # STOP_POINT_ID can be found using Navitia API, see
    # https://canaltp.github.io/navitia-playground/play.html?request=https%3A%2F%2Fapi.navitia.io%2Fv1%2Fcoverage%2Ffr-idf%2Flines%2Fline%253AOIF%253A100100068%253A68OIF442%2Fstop_areas%3F
    STOP_POINT_ID = 'stopPoint:59:5138213'  # "Maurice Arnoux" stop
    API_KEY = 'PUT_YOUR_API_KEY_HERE'
    r = requests.get(
        'https://api-lab-trone-stif.opendata.stif.info/service/tr-vianavigo/departures?line_id=%s&stop_point_id=%s&apikey=%s'
        % (LINE_ID, STOP_POINT_ID, API_KEY)
    )
    try:
        r.raise_for_status()
    except requests.exceptions.HTTPError:
        if DEBUG:
            raise
        # Ignore
        sys.exit()
    j = r.json()

    for item in j:
        print(item)
        r = DataPoint(
            lineDirection=item['lineDirection'],
            code=item['code'],
            time=item.get('time', item.get('schedule'))
        )
        r.save()
    db.close()
