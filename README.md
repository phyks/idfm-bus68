RATP Bus 68 OpenData
====================

Here you will find the code I used to analyze and plot some waiting times for
the bus 68 operated by RATP in Paris. Feel free to reuse and adapt with other
lines.

Blog post is available at [https://phyks.me/2018/12/suivi-des-passages-de-bus-a-partir-des-donnees-ouvertes-didfm.html](https://phyks.me/2018/12/suivi-des-passages-de-bus-a-partir-des-donnees-ouvertes-didfm.html#suivi-des-passages-de-bus-a-partir-des-donnees-ouvertes-didfm).

## Files included

* `bus_68_datapoints.py` is the data collection script (to be run by a
    crontask for instance).
* `bus_68_datapoints.sql` is a SQL dump of the SQLite database I have storing
    the data I collected. The data was collected from [the STIF
    API](https://api-lab-trone-stif.opendata.stif.info/pages/api-test-temps-reel/)
    (ODbL license).
* `Plot.ipynb` is a Jupyter Notebook to analyze the data.


## License

This code is licensed under an MIT license.
